import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { EstimatesRoutingModule } from './estimates-routing.module';
import { EstimatesComponent } from './estimates.component';
import { HomeComponent } from './pages/home/home.component';
import { EstimateComponent } from './pages/estimate/estimate.component';

@NgModule({
	declarations: [EstimatesComponent, HomeComponent, EstimateComponent],
	imports: [CommonModule, SharedModule, MaterialModule, EstimatesRoutingModule],
})
export class EstimatesModule {}
