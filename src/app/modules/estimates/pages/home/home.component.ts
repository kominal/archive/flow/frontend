import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { PaginationRequest } from '@kominal/lib-angular-pagination';
import { TableComponent } from '@kominal/lib-angular-table';
import { Estimate } from 'src/app/core/models/estimate';
import { EstimateService } from 'src/app/core/services/estimate/estimate.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements AfterViewInit {
	@ViewChild(TableComponent) table!: TableComponent<Estimate>;

	displayedColumns: string[] = ['name', 'actions'];

	constructor(public tenantService: TenantService, private estimateService: EstimateService) {}

	ngAfterViewInit() {
		this.tenantService.currentTenantIdSubject.subscribe(() => {
			this.table.requestUpdate();
		});
	}

	async updateData(paginationRequest: PaginationRequest): Promise<void> {
		this.table.data.next(await this.estimateService.list(paginationRequest));
	}

	async delete(estimate: Estimate) {
		if (await this.estimateService.delete(estimate)) {
			this.table.requestUpdate();
		}
	}
}
