import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormModule } from '@kominal/lib-angular-form';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './pages/home/home.component';
import { ImportComponent } from './pages/import/import.component';
import { TransactionComponent } from './pages/transaction/transaction.component';
import { TransactionsRoutingModule } from './transactions-routing.module';
import { TransactionsComponent } from './transactions.component';
import { ImportsComponent } from './pages/imports/imports.component';

@NgModule({
	declarations: [TransactionsComponent, HomeComponent, TransactionComponent, ImportComponent, ImportsComponent],
	imports: [CommonModule, TransactionsRoutingModule, SharedModule, MaterialModule, FormModule],
})
export class TransactionsModule {}
