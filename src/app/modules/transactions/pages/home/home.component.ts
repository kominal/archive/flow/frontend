import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { TableComponent } from '@kominal/lib-angular-table';
import { Transaction } from 'src/app/core/models/transaction';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';
import { TransactionService } from 'src/app/core/services/transaction/transaction.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements AfterViewInit {
	@ViewChild(TableComponent) table!: TableComponent<Transaction>;

	autoColumns: string[] = ['time'];
	displayedColumns: string[] = ['internalChange', 'time', 'debtorAccount', 'creditorAccount', 'amount', 'actions'];

	constructor(public tenantService: TenantService, public transactionService: TransactionService) {}

	ngAfterViewInit() {
		this.tenantService.currentTenantIdSubject.subscribe(() => {
			this.table.requestUpdate();
		});
	}

	async delete(transaction: Transaction) {
		if (await this.transactionService.delete(transaction)) {
			this.table.requestUpdate();
		}
	}

	getInternalChangeIcon(internalChange: string): string {
		if (internalChange === 'POSITIVE') {
			return 'arrow_upward';
		} else if (internalChange === 'NEGATIVE') {
			return 'arrow_downward';
		}
		return 'remove';
	}
}
