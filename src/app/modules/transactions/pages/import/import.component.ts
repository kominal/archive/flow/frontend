import { Component, ElementRef, HostListener, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { LogService } from '@kominal/lib-angular-logging';
import { BehaviorSubject } from 'rxjs';
import { Account } from 'src/app/core/models/account';
import { AccountService } from 'src/app/core/services/account/account.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';
import { TransactionsImportJobService } from 'src/app/core/services/transactions-import/transactions-import-job.service';
import { iconManifest } from './icon-manifest';

@Component({
	selector: 'app-import',
	templateUrl: './import.component.html',
	styleUrls: ['./import.component.scss'],
})
export class ImportComponent {
	public Object = Object;

	public loading = false;
	public form: FormGroup;

	public accountsSubject = new BehaviorSubject<Account[]>([]);

	@ViewChild('fileInput') fileInput!: ElementRef<HTMLInputElement>;

	public files: { file: File; preview?: SafeUrl }[] = [];
	public dragging = false;

	constructor(
		public tenantService: TenantService,
		private transactionsImportJobService: TransactionsImportJobService,
		private logService: LogService,
		private domSanitizer: DomSanitizer,
		accountService: AccountService
	) {
		this.form = new FormGroup({
			_id: new FormControl(),
			accountId: new FormControl(undefined, Validators.required),
		});

		accountService.list({}).then((page) => this.accountsSubject.next(page.items));
	}

	async onSubmit() {
		this.loading = true;
		try {
			await this.transactionsImportJobService.create(
				this.form.value,
				this.files.map((f) => f.file)
			);
			this.navigateToParent();
		} catch (e) {
			this.logService.handleError(e);
			this.loading = false;
		}
	}

	navigateToParent() {
		this.tenantService.navigateToTenant(['transactions', 'imports']);
	}

	async addFiles(files: File[]) {
		for (const file of files) {
			const search = this.files.find(
				(value) => value.file.name === file.name && value.file.size === file.size && value.file.type === file.type
			);

			if (search) {
				this.files.splice(this.files.indexOf(search), 1);
			}

			let preview;
			if (file.type.startsWith('image/')) {
				preview = this.domSanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file));
			} else {
				const extension = file.name.substring(file.name.lastIndexOf('.') + 1);

				let urlString = iconManifest.find(
					(iconScheme) => iconScheme.extensions.includes(extension) || iconScheme.extensions.includes(file.type)
				)?.icon;

				preview = this.domSanitizer.bypassSecurityTrustUrl(urlString || './assets/images/file-icons/default_file.svg');
			}

			this.files.unshift({
				file,
				preview,
			});
		}
	}

	removeFile(index: number) {
		this.files.splice(index, 1);
	}

	@HostListener('dragover', ['$event']) onDragOver(e: Event) {
		e.preventDefault();
		e.stopPropagation();
		this.dragging = true;
	}

	@HostListener('dragleave', ['$event']) public onDragLeave(e: Event) {
		e.preventDefault();
		e.stopPropagation();
		this.dragging = false;
	}

	@HostListener('drop', ['$event']) public ondrop(e: any) {
		e.preventDefault();
		e.stopPropagation();
		this.dragging = false;
		this.addFiles(e.dataTransfer.files);
	}
}
