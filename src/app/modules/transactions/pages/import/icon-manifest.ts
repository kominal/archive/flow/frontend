export const iconManifest = [
	{
		icon: 'assets/images/file-icons/file_type_word.svg',
		extensions: ['doc', 'dot', 'wbk', 'docx', 'docm', 'dotx', 'dotm', 'docb'],
	},
	{
		icon: 'assets/images/file-icons/file_type_excel.svg',
		extensions: ['xls', 'xlt', 'xlm', 'xlsx', 'xlsm', 'xltx', 'xltm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw'],
	},
	{
		icon: 'assets/images/file-icons/file_type_powerpoint.svg',
		extensions: ['ppt', 'pot', 'pps', 'pptx', 'pptm', 'potx', 'potm', 'ppam', 'ppsx', 'ppsm', 'sldx', 'sldm'],
	},
	{
		icon: 'assets/images/file-icons/file_type_pdf2.svg',
		extensions: ['pdf'],
	},
	{
		icon: 'assets/images/file-icons/file_type_xml.svg',
		extensions: ['xml', 'html'],
	},
	{
		icon: 'assets/images/file-icons/file_type_text.svg',
		extensions: ['txt'],
	},
	{
		icon: 'assets/images/file-icons/file_type_audio.svg',
		extensions: ['audio/'],
	},
	{
		icon: 'assets/images/file-icons/file_type_video.svg',
		extensions: ['video/'],
	},
];
