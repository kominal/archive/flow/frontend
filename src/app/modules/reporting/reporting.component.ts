import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-reporting',
	templateUrl: './reporting.component.html',
	styleUrls: ['./reporting.component.scss'],
})
export class ReportingComponent {
	constructor(activatedRoute: ActivatedRoute, tenantService: TenantService) {
		activatedRoute.params.subscribe((params) => {
			tenantService.currentTenantIdSubject.next(params.tenantId);
		});
	}
}
