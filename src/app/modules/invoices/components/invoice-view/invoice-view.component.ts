import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
	selector: 'app-invoice-view',
	templateUrl: './invoice-view.component.html',
	styleUrls: ['./invoice-view.component.scss'],
})
export class InvoiceViewComponent implements OnInit {
	autoColumns: string[] = ['description', 'rate', 'quantity', 'line-total', 'actions'];
	dataSource = new MatTableDataSource(<any>[]);
	myControl = new FormControl();
	options: string[] = ['One', 'Two', 'Three'];
	filteredOptions!: Observable<string[]>;

	constructor() {}

	ngOnInit(): void {
		this.filteredOptions = this.myControl.valueChanges.pipe(
			startWith(''),
			map((value) => this._filter(value))
		);
	}

	private _filter(value: string): string[] {
		const filterValue = value.toLowerCase();

		return this.options.filter((option) => option.toLowerCase().indexOf(filterValue) === 0);
	}

	drop(event: CdkDragDrop<any>) {
		console.log(event);

		moveItemInArray(this.dataSource.data, event.previousIndex, event.currentIndex);
		this.dataSource.data = [...this.dataSource.data];
	}

	createRow() {
		this.dataSource.data.push({
			description: `Description ${this.dataSource.data.length}`,
			rate: `rate ${this.dataSource.data.length}`,
			quantity: `quantity ${this.dataSource.data.length}`,
			total: `line-total ${this.dataSource.data.length}`,
		});

		this.dataSource.data = [...this.dataSource.data];
	}

	removePosition(element: any) {
		this.dataSource.data.splice(this.dataSource.data.indexOf(element), 1);
		this.dataSource.data = [...this.dataSource.data];
	}
}
