import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AdvancedComponent } from './pages/advanced/advanced.component';
import { MasterDataComponent } from './pages/master-data/master-data.component';
import { SettingsRoutingModule } from './settings-routing.module';
import { SettingsComponent } from './settings.component';

@NgModule({
	declarations: [SettingsComponent, AdvancedComponent, MasterDataComponent],
	imports: [CommonModule, SharedModule, MaterialModule, SettingsRoutingModule],
})
export class SettingsModule {}
