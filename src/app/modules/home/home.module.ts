import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { GdprComponent } from './pages/gdpr/gdpr.component';
import { HomeComponent as HomeComponent2 } from './pages/home/home.component';
import { ImprintComponent } from './pages/imprint/imprint.component';

@NgModule({
	declarations: [HomeComponent, HomeComponent2, ImprintComponent, GdprComponent],
	imports: [CommonModule, SharedModule, MaterialModule, HomeRoutingModule],
})
export class HomeModule {}
