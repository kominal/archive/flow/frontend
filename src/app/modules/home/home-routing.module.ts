import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GdprComponent } from './pages/gdpr/gdpr.component';
import { HomeComponent } from './pages/home/home.component';
import { ImprintComponent } from './pages/imprint/imprint.component';

const routes: Routes = [
	{ path: '', component: HomeComponent },
	{ path: 'imprint', component: ImprintComponent },
	{ path: 'gdpr', component: GdprComponent },
	{ path: '**', redirectTo: '' },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class HomeRoutingModule {}
