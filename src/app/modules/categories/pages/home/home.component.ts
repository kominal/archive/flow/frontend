import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { PaginationRequest } from '@kominal/lib-angular-pagination';
import { TableComponent } from '@kominal/lib-angular-table';
import { Category } from 'src/app/core/models/category';
import { CategoryService } from 'src/app/core/services/category/category.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements AfterViewInit {
	@ViewChild(TableComponent) table!: TableComponent<Category>;

	displayedColumns: string[] = ['name', 'actions'];

	constructor(public tenantService: TenantService, private categoryService: CategoryService) {}

	ngAfterViewInit() {
		this.tenantService.currentTenantIdSubject.subscribe(() => {
			this.table.requestUpdate();
		});
	}

	async updateData(paginationRequest: PaginationRequest): Promise<void> {
		this.table.data.next(await this.categoryService.list(paginationRequest));
	}

	async delete(category: Category) {
		if (await this.categoryService.delete(category)) {
			this.table.requestUpdate();
		}
	}
}
