import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { CategoriesRoutingModule } from './categories-routing.module';
import { CategoriesComponent } from './categories.component';
import { CategoryComponent } from './pages/category/category.component';
import { HomeComponent } from './pages/home/home.component';

@NgModule({
	declarations: [CategoriesComponent, HomeComponent, CategoryComponent],
	imports: [CommonModule, SharedModule, MaterialModule, CategoriesRoutingModule],
})
export class CategoriesModule {}
