import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { CreateTenantComponent } from './pages/create-tenant/create-tenant.component';
import { HomeComponent } from './pages/home/home.component';

@NgModule({
	declarations: [DashboardComponent, HomeComponent, CreateTenantComponent],
	imports: [CommonModule, SharedModule, MaterialModule, DashboardRoutingModule],
})
export class DashboardModule {}
