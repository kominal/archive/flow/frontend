import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LogService } from '@kominal/lib-angular-logging';
import { currenciesSubject } from 'src/app/core/models/currencies';
import { Tenant } from 'src/app/core/models/tenant';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-create-tenant',
	templateUrl: './create-tenant.component.html',
	styleUrls: ['./create-tenant.component.scss'],
})
export class CreateTenantComponent {
	public Object = Object;
	public currenciesSubject = currenciesSubject;

	public loading = false;
	public form: FormGroup;

	constructor(public tenantService: TenantService, private logService: LogService, private router: Router) {
		this.form = this.createFormGroup();
		this.tenantService.currentTenantSubject.subscribe((tenant) => {
			this.form = this.createFormGroup(tenant);
		});
	}

	createFormGroup(tenant?: Tenant): FormGroup {
		return new FormGroup({
			_id: new FormControl(tenant?._id),
			name: new FormControl(tenant?.name, Validators.required),
			currency: new FormControl(tenant?.currency, Validators.required),
		});
	}

	async onSubmit() {
		this.loading = true;
		try {
			const { _id } = await this.tenantService.create(this.form.value);
			await this.tenantService.refreshTenants();
			this.router.navigate(['/tenant/', _id, 'dashboard']);
		} catch (e) {
			this.logService.handleError(e);
		}
		this.loading = false;
	}
}
