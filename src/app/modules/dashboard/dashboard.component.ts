import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent {
	constructor(activatedRoute: ActivatedRoute, tenantService: TenantService) {
		activatedRoute.params.subscribe((params) => {
			tenantService.currentTenantIdSubject.next(params.tenantId);
		});
	}
}
