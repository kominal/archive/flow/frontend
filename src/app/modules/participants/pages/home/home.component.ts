import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { TableComponent } from '@kominal/lib-angular-table';
import { Participant } from 'src/app/core/models/participant';
import { ParticipantService } from 'src/app/core/services/participant/participant.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements AfterViewInit {
	@ViewChild(TableComponent) table!: TableComponent<Participant>;

	autoColumns: string[] = ['name'];
	displayedColumns: string[] = ['name', 'actions'];

	constructor(public tenantService: TenantService, public participantService: ParticipantService) {}

	ngAfterViewInit() {
		this.tenantService.currentTenantIdSubject.subscribe(() => {
			this.table.requestUpdate();
		});
	}

	async delete(participant: Participant) {
		if (await this.participantService.delete(participant)) {
			this.table.requestUpdate();
		}
	}
}
