import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './pages/home/home.component';
import { ParticipantsRoutingModule } from './participants-routing.module';
import { ParticipantsComponent } from './participants.component';
import { ParticipantComponent } from './pages/participant/participant.component';

@NgModule({
	declarations: [ParticipantsComponent, HomeComponent, ParticipantComponent],
	imports: [CommonModule, SharedModule, MaterialModule, ParticipantsRoutingModule],
})
export class ParticipantsModule {}
