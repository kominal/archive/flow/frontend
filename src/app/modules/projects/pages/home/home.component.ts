import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { PaginationRequest } from '@kominal/lib-angular-pagination';
import { TableComponent } from '@kominal/lib-angular-table';
import { Project } from 'src/app/core/models/project';
import { ProjectService } from 'src/app/core/services/project/project.service';
import { TenantService } from 'src/app/core/services/tenant/tenant.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements AfterViewInit {
	@ViewChild(TableComponent) table!: TableComponent<Project>;

	autoColumns = ['name'];
	displayedColumns = ['name', 'actions'];

	constructor(public tenantService: TenantService, private projectService: ProjectService) {}

	ngAfterViewInit() {
		this.tenantService.currentTenantIdSubject.subscribe(() => {
			this.table.requestUpdate();
		});
	}

	async updateData(paginationRequest: PaginationRequest): Promise<void> {
		this.table.data.next(await this.projectService.list(paginationRequest));
	}

	async delete(project: Project) {
		if (await this.projectService.delete(project)) {
			this.table.requestUpdate();
		}
	}
}
