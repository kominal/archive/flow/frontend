import { Component, Inject } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
	selector: 'app-permissions-dialog',
	templateUrl: './permissions-dialog.component.html',
	styleUrls: ['./permissions-dialog.component.scss'],
})
export class PermissionsDialogComponent {
	form: FormGroup;

	availablePermissions = ['core.user-service.write', 'core.user-service.read', 'flow.controller.tenant.settings'];

	constructor(public dialogRef: MatDialogRef<PermissionsDialogComponent>, @Inject(MAT_DIALOG_DATA) public permissions: string[]) {
		this.form = new FormGroup({
			permissions: new FormControl(permissions),
		});
	}

	onSubmit() {
		this.dialogRef.close(this.form.value.permissions);
	}
}
