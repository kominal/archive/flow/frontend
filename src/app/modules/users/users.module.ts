import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TableModule } from '@kominal/lib-angular-table';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { HomeComponent } from './pages/home/home.component';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { PermissionsDialogComponent } from './components/permissions-dialog/permissions-dialog.component';
import { AddUserDialogComponent } from './components/add-user-dialog/add-user-dialog.component';

@NgModule({
	declarations: [HomeComponent, UsersComponent, PermissionsDialogComponent, AddUserDialogComponent],
	imports: [CommonModule, UsersRoutingModule, SharedModule, MaterialModule, TableModule],
})
export class UsersModule {}
