import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DialogModule } from '@kominal/lib-angular-dialog';
import { FormModule } from '@kominal/lib-angular-form';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from 'src/app/shared/material.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { AccountsRoutingModule } from './accounts-routing.module';
import { AccountsComponent } from './accounts.component';
import { AccountComponent } from './pages/account/account.component';
import { HomeComponent } from './pages/home/home.component';

@NgModule({
	declarations: [AccountsComponent, HomeComponent, AccountComponent],
	imports: [CommonModule, SharedModule, MaterialModule, AccountsRoutingModule, TranslateModule, DialogModule, FormModule],
})
export class AccountsModule {}
