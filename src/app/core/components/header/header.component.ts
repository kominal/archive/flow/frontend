import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@kominal/core-user-service-angular-client';
import { LayoutService } from '@kominal/lib-angular-layout';
import { ActionsService } from '../../services/actions/actions.service';
import { TenantService } from '../../services/tenant/tenant.service';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
	constructor(
		public router: Router,
		public layoutService: LayoutService,
		public userService: UserService,
		public tenantService: TenantService,
		public actionsService: ActionsService
	) {}
}
