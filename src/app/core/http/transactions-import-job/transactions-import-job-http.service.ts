import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '@kominal/core-user-service-angular-client';
import { PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/lib-angular-pagination';
import { TransactionsImportJob } from '../../models/transactionsImportJob';

@Injectable({
	providedIn: 'root',
})
export class TransactionsImportJobHttpService {
	constructor(private httpClient: HttpClient) {}

	list(tenantId: string, paginationRequest: PaginationRequest): Promise<PaginationResponse<TransactionsImportJob>> {
		return this.httpClient
			.get<PaginationResponse<TransactionsImportJob>>(`/controller/${tenantId}/transactionsImportJobs`, {
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	get(tenantId: string, transactionsImportJobId: string): Promise<TransactionsImportJob> {
		return this.httpClient
			.get<TransactionsImportJob>(
				`/controller/${tenantId}/transactionsImportJobs/${transactionsImportJobId}`,
				AUTHENTICATION_REQUIRED_OPTIONS
			)
			.toPromise();
	}

	create(tenantId: string, transactionsImportJob: TransactionsImportJob, files: File[]): Promise<{ _id: string }> {
		const formData = new FormData();
		for (const index in files) {
			formData.append(`files[${index}]`, files[index]);
		}
		for (const key of Object.keys(transactionsImportJob)) {
			formData.append(key, (transactionsImportJob as any)[key]);
		}

		return this.httpClient
			.post<{ _id: string }>(`/controller/${tenantId}/transactionsImportJobs`, formData, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	update(tenantId: string, transactionsImportJob: TransactionsImportJob): Promise<void> {
		return this.httpClient
			.put<void>(
				`/controller/${tenantId}/transactionsImportJobs/${transactionsImportJob._id}`,
				transactionsImportJob,
				AUTHENTICATION_REQUIRED_OPTIONS
			)
			.toPromise();
	}

	delete(tenantId: string, transactionsImportJobId: string): Promise<void> {
		return this.httpClient
			.delete<void>(`/controller/${tenantId}/transactionsImportJobs/${transactionsImportJobId}`, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}
}
