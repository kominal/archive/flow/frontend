import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '@kominal/core-user-service-angular-client';
import { PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/lib-angular-pagination';
import { Participant } from '../../models/participant';

@Injectable({
	providedIn: 'root',
})
export class ParticipantHttpService {
	constructor(private httpClient: HttpClient) {}

	list(tenantId: string, paginationRequest: PaginationRequest): Promise<PaginationResponse<Participant>> {
		return this.httpClient
			.get<PaginationResponse<Participant>>(`/controller/${tenantId}/participants`, {
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	get(tenantId: string, participantId: string): Promise<Participant> {
		return this.httpClient
			.get<Participant>(`/controller/${tenantId}/participants/${participantId}`, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	create(tenantId: string, participant: Participant): Promise<{ _id: string }> {
		return this.httpClient
			.post<{ _id: string }>(`/controller/${tenantId}/participants`, participant, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	update(tenantId: string, participant: Participant): Promise<void> {
		return this.httpClient
			.put<void>(`/controller/${tenantId}/participants/${participant._id}`, participant, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	delete(tenantId: string, participantId: string): Promise<void> {
		return this.httpClient
			.delete<void>(`/controller/${tenantId}/participants/${participantId}`, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}
}
