import { TestBed } from '@angular/core/testing';

import { ParticipantHttpService } from './participant-http.service';

describe('ParticipantHttpService', () => {
  let service: ParticipantHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParticipantHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
