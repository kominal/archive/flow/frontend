import { TestBed } from '@angular/core/testing';

import { TenantHttpService } from './tenant-http.service';

describe('TenantHttpService', () => {
  let service: TenantHttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TenantHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
