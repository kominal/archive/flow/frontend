import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '@kominal/core-user-service-angular-client';
import { PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/lib-angular-pagination';
import { Category } from '../../models/category';

@Injectable({
	providedIn: 'root',
})
export class CategoryHttpService {
	constructor(private httpClient: HttpClient) {}

	list(tenantId: string, paginationRequest: PaginationRequest): Promise<PaginationResponse<Category>> {
		return this.httpClient
			.get<PaginationResponse<Category>>(`/controller/${tenantId}/categories`, {
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	get(tenantId: string, categoryId: string): Promise<Category> {
		return this.httpClient.get<Category>(`/controller/${tenantId}/categories/${categoryId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	create(tenantId: string, category: Category): Promise<{ _id: string }> {
		return this.httpClient
			.post<{ _id: string }>(`/controller/${tenantId}/categories`, category, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	update(tenantId: string, category: Category): Promise<void> {
		return this.httpClient
			.put<void>(`/controller/${tenantId}/categories/${category._id}`, category, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	delete(tenantId: string, categoryId: string): Promise<void> {
		return this.httpClient.delete<void>(`/controller/${tenantId}/categories/${categoryId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}
}
