import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AUTHENTICATION_REQUIRED, AUTHENTICATION_REQUIRED_OPTIONS } from '@kominal/core-user-service-angular-client';
import { PaginationRequest, PaginationResponse, toPaginationParams } from '@kominal/lib-angular-pagination';
import { Account } from '../../models/account';

@Injectable({
	providedIn: 'root',
})
export class AccountHttpService {
	constructor(private httpClient: HttpClient) {}

	list(tenantId: string, paginationRequest: PaginationRequest): Promise<PaginationResponse<Account>> {
		return this.httpClient
			.get<PaginationResponse<Account>>(`/controller/${tenantId}/accounts`, {
				params: toPaginationParams(paginationRequest),
				headers: AUTHENTICATION_REQUIRED,
			})
			.toPromise();
	}

	get(tenantId: string, accountId: string): Promise<Account> {
		return this.httpClient.get<Account>(`/controller/${tenantId}/accounts/${accountId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	create(tenantId: string, account: Account): Promise<{ _id: string }> {
		return this.httpClient.post<{ _id: string }>(`/controller/${tenantId}/accounts`, account, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}

	update(tenantId: string, account: Account): Promise<void> {
		return this.httpClient
			.put<void>(`/controller/${tenantId}/accounts/${account._id}`, account, AUTHENTICATION_REQUIRED_OPTIONS)
			.toPromise();
	}

	delete(tenantId: string, accountId: string): Promise<void> {
		return this.httpClient.delete<void>(`/controller/${tenantId}/accounts/${accountId}`, AUTHENTICATION_REQUIRED_OPTIONS).toPromise();
	}
}
