import { Injectable } from '@angular/core';
import { LogService } from '@kominal/lib-angular-logging';
import { DashboardHttpService } from '../../http/dashboard/dashboard-http.service';
import { Dashboard } from '../../models/dashboard';
import { TenantService } from '../tenant/tenant.service';

@Injectable({
	providedIn: 'root',
})
export class DashboardService {
	constructor(private dashboardHttpService: DashboardHttpService, private tenantService: TenantService, private logService: LogService) {}

	async get(): Promise<Dashboard | undefined> {
		try {
			return await this.dashboardHttpService.get(this.tenantService.getCurrentTenantId());
		} catch (e) {
			this.logService.handleError(e);
		}
	}
}
