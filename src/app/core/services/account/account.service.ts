import { Injectable } from '@angular/core';
import { DialogService } from '@kominal/lib-angular-dialog';
import { LogService } from '@kominal/lib-angular-logging';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { AccountHttpService } from '../../http/account/account-http.service';
import { Account } from '../../models/account';
import { TenantService } from '../tenant/tenant.service';

@Injectable({
	providedIn: 'root',
})
export class AccountService {
	constructor(
		private accountHttpService: AccountHttpService,
		private tenantService: TenantService,
		private dialogService: DialogService,
		private logService: LogService
	) {}

	async list(paginationRequest: PaginationRequest): Promise<PaginationResponse<Account>> {
		try {
			return await this.accountHttpService.list(this.tenantService.getCurrentTenantId(), paginationRequest);
		} catch (e) {
			this.logService.handleError(e);
		}
		return EMPTY_PAGINATION_RESPONSE;
	}

	async get(accountId: string): Promise<Account | undefined> {
		try {
			return await this.accountHttpService.get(this.tenantService.getCurrentTenantId(), accountId);
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	create(account: Account): Promise<{ _id: string }> {
		return this.accountHttpService.create(this.tenantService.getCurrentTenantId(), account);
	}

	update(account: Account): Promise<void> {
		return this.accountHttpService.update(this.tenantService.getCurrentTenantId(), account);
	}

	createOrUpdate(account: Account): Promise<void | { _id: string }> {
		return account._id ? this.update(account) : this.create(account);
	}

	async delete(account: Account): Promise<boolean> {
		try {
			if (account._id && (await this.dialogService.openConfirmDeleteDialog(account._id))) {
				await this.accountHttpService.delete(this.tenantService.getCurrentTenantId(), account._id);
				return true;
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		return false;
	}
}
