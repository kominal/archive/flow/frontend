import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '@kominal/core-user-service-angular-client';
import { DialogService } from '@kominal/lib-angular-dialog';
import { LogService } from '@kominal/lib-angular-logging';
import { BehaviorSubject, of } from 'rxjs';
import { mergeAll, skip } from 'rxjs/operators';
import { TenantHttpService } from '../../http/tenant/tenant-http.service';
import { Tenant } from '../../models/tenant';

@Injectable({
	providedIn: 'root',
})
export class TenantService {
	public currentTenantIdSubject = new BehaviorSubject<string | undefined>(undefined);
	public currentTenantSubject = new BehaviorSubject<Tenant | undefined>(undefined);
	public tenantsSubject = new BehaviorSubject<Tenant[]>([]);
	public updateTenants = true;

	constructor(
		private router: Router,
		private userService: UserService,
		private tenantHttpService: TenantHttpService,
		private logService: LogService,
		private dialogService: DialogService
	) {
		of(this.currentTenantIdSubject, this.tenantsSubject)
			.pipe(mergeAll(), skip(1))
			.subscribe(() => {
				const tenantId = this.currentTenantIdSubject.value;

				if (tenantId) {
					userService.currentTenantSubject.next({ _id: tenantId });
					this.currentTenantSubject.next(this.tenantsSubject.value.find((t) => t._id === tenantId));
				} else {
					userService.currentTenantSubject.next(undefined);
					this.currentTenantSubject.next(undefined);
				}
			});
		userService.tokenSubject.subscribe(async (token) => {
			if (token) {
				const tenantIds = Object.keys(token.payload.tenants);

				let requireTenantUpdate = this.updateTenants;
				this.updateTenants = false;

				if (!requireTenantUpdate) {
					if (this.tenantsSubject.value.length != tenantIds.length) {
						for (const tenantId of tenantIds) {
							if (!this.tenantsSubject.value.find((t) => t._id === tenantId)) {
								requireTenantUpdate = true;
								break;
							}
						}
					}
				}

				if (requireTenantUpdate) {
					this.tenantsSubject.next(await this.list());
				}

				if (this.currentTenantIdSubject.value) {
					this.currentTenantSubject.next(this.tenantsSubject.value.find((t) => t._id === this.currentTenantIdSubject.value));
				}
			} else {
				this.currentTenantSubject.next(undefined);
				this.tenantsSubject.next([]);
			}
		});
	}

	async list(): Promise<Tenant[]> {
		try {
			return await this.tenantHttpService.list();
		} catch (e) {
			this.logService.error(e);
		}
		return [];
	}

	async get(tenantId: string): Promise<Tenant | undefined> {
		try {
			return await this.tenantHttpService.get(tenantId);
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	create(tenant: Tenant): Promise<{ _id: string }> {
		return this.tenantHttpService.create(tenant);
	}

	update(tenant: Tenant): Promise<void> {
		return this.tenantHttpService.update(tenant);
	}

	async delete(tenant: Tenant): Promise<boolean> {
		try {
			if (tenant._id && (await this.dialogService.openConfirmDeleteDialog(tenant._id))) {
				await this.tenantHttpService.delete(tenant._id);
				await this.refreshTenants();
				return true;
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		return false;
	}

	async refreshTenants() {
		this.updateTenants = true;
		await this.userService.refresh();
	}

	getCurrentTenantId(): string {
		if (!this.currentTenantIdSubject.value) {
			throw new Error('transl.error.notSelected.tenant');
		}
		return this.currentTenantIdSubject.value;
	}

	navigateToTenant(command: string[]) {
		if (this.currentTenantSubject.value) {
			this.router.navigate(['/tenant/', this.currentTenantIdSubject.value, ...command]);
		} else if (this.tenantsSubject.value.length > 0) {
			this.router.navigate(['/tenant/', this.tenantsSubject.value[0]._id, ...command]);
		} else {
			this.router.navigate(['/dashboard']);
		}
	}
}
