import { TestBed } from '@angular/core/testing';

import { TransactionsImportJobService } from './transactions-import-job.service';

describe('TransactionsImportJobService', () => {
  let service: TransactionsImportJobService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransactionsImportJobService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
