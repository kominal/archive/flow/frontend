import { Injectable } from '@angular/core';
import { DialogService } from '@kominal/lib-angular-dialog';
import { LogService } from '@kominal/lib-angular-logging';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { TransactionHttpService } from '../../http/transaction/transaction-http.service';
import { Transaction } from '../../models/transaction';
import { TenantService } from '../tenant/tenant.service';

@Injectable({
	providedIn: 'root',
})
export class TransactionService {
	constructor(
		private transactionHttpService: TransactionHttpService,
		private tenantService: TenantService,
		private dialogService: DialogService,
		private logService: LogService
	) {}

	async list(paginationRequest: PaginationRequest): Promise<PaginationResponse<Transaction>> {
		try {
			return await this.transactionHttpService.list(this.tenantService.getCurrentTenantId(), paginationRequest);
		} catch (e) {
			this.logService.handleError(e);
		}
		return EMPTY_PAGINATION_RESPONSE;
	}

	async get(transactionId: string): Promise<Transaction | undefined> {
		try {
			return await this.transactionHttpService.get(this.tenantService.getCurrentTenantId(), transactionId);
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	create(transaction: Transaction): Promise<{ _id: string }> {
		return this.transactionHttpService.create(this.tenantService.getCurrentTenantId(), transaction);
	}

	update(transaction: Transaction): Promise<void> {
		return this.transactionHttpService.update(this.tenantService.getCurrentTenantId(), transaction);
	}

	createOrUpdate(transaction: Transaction): Promise<void | { _id: string }> {
		return transaction._id ? this.update(transaction) : this.create(transaction);
	}

	async delete(transaction: Transaction): Promise<boolean> {
		try {
			if (transaction._id && (await this.dialogService.openConfirmDeleteDialog(transaction._id))) {
				await this.transactionHttpService.delete(this.tenantService.getCurrentTenantId(), transaction._id);
				return true;
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		return false;
	}
}
