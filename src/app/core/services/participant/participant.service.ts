import { Injectable } from '@angular/core';
import { DialogService } from '@kominal/lib-angular-dialog';
import { LogService } from '@kominal/lib-angular-logging';
import { EMPTY_PAGINATION_RESPONSE, PaginationRequest, PaginationResponse } from '@kominal/lib-angular-pagination';
import { ParticipantHttpService } from '../../http/participant/participant-http.service';
import { Participant } from '../../models/participant';
import { TenantService } from '../tenant/tenant.service';

@Injectable({
	providedIn: 'root',
})
export class ParticipantService {
	constructor(
		private participantHttpService: ParticipantHttpService,
		private tenantService: TenantService,
		private dialogService: DialogService,
		private logService: LogService
	) {}

	async list(paginationRequest: PaginationRequest): Promise<PaginationResponse<Participant>> {
		try {
			return await this.participantHttpService.list(this.tenantService.getCurrentTenantId(), paginationRequest);
		} catch (e) {
			this.logService.handleError(e);
		}
		return EMPTY_PAGINATION_RESPONSE;
	}

	async get(participantId: string): Promise<Participant | undefined> {
		try {
			return await this.participantHttpService.get(this.tenantService.getCurrentTenantId(), participantId);
		} catch (e) {
			this.logService.handleError(e);
		}
	}

	create(participant: Participant): Promise<{ _id: string }> {
		return this.participantHttpService.create(this.tenantService.getCurrentTenantId(), participant);
	}

	update(participant: Participant): Promise<void> {
		return this.participantHttpService.update(this.tenantService.getCurrentTenantId(), participant);
	}

	createOrUpdate(participant: Participant): Promise<void | { _id: string }> {
		return participant._id ? this.update(participant) : this.create(participant);
	}

	async delete(participant: Participant): Promise<boolean> {
		try {
			if (participant._id && (await this.dialogService.openConfirmDeleteDialog(participant._id))) {
				await this.participantHttpService.delete(this.tenantService.getCurrentTenantId(), participant._id);
				return true;
			}
		} catch (e) {
			this.logService.handleError(e);
		}
		return false;
	}
}
