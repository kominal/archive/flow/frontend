export interface Category {
	_id?: string;
	tenantId?: string;
	name: string;
}
