export interface TransactionsImportJob {
	_id?: string;
	tenantId: string;
	created: Date;
	type: string;
	accountId: string;
	from: Date;
	to: Date;
	started?: Date;
	completed?: Date;
	status?: string;
	successful?: number;
	failed?: number;
	reason?: string;
}
