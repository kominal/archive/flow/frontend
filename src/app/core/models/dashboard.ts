export interface Dashboard {
	revenue: { [key: string]: number };
	expenses: { [key: string]: number };
}
