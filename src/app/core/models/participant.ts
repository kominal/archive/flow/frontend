export interface Participant {
	_id?: string;
	tenantId: string;
	name: string;
}
