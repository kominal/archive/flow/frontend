export interface Tenant {
	_id?: string;
	name: string;
	currency: string;
}
