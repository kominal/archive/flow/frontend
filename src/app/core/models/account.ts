import { BehaviorSubject } from 'rxjs';

export interface Account {
	_id?: string;
	tenantId: string;
	ownerId: string;
	created: Date;
	name: string;
	internal: boolean;
	participantId: string;
	type: string;
	bank?: string;
	iban?: string;
	username?: string;
	password?: string;
	email?: string;
}

export const accountTypesSubject = new BehaviorSubject<string[]>(['BANK', 'PAYPAL', 'DIRECT_DEBIT_TARGET', 'OTHER']);
