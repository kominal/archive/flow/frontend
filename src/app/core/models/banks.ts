import { BehaviorSubject } from 'rxjs';

export const banksSubject = new BehaviorSubject<string[]>(['KREISSPARKASSE_KOELN', 'STADTSPARKASSE_MUENCHEN']);
