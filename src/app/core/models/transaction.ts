export interface Transaction {
	_id?: string;
	tenantId: string;
	categoryIds?: string[];
	projectIds?: string[];
	invoiceIds?: string[];
	debtorAccountId: string; //Sender
	creditorAccountId: string; //Receiver
	type: string;
	time: Date;
	amount: number;
	currency: string;
	purpose?: string;
	mandateReference?: string;
}
