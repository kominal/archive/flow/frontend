export interface Project {
	_id?: string;
	tenantId: string;
	name: string;
}
